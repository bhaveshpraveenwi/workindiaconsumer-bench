### Sync
How benchmark the sync version.

1. Run the server
```gunicorn workindiaconsumer.wsgi -b 0.0.0.0:9008 --workers 10 --limit-request-line 4094 --limit-request-fields 100 --timeout 300 --log-level debug```

2. Start the consumer
```sh
python manage.py sync_task

```

3. Now run the script to produce 1000 messages
```sh
python manage.py shell
```

Once inside the shell, run the following commands

- `from workindiaconsumer.utils import *`
- `main()`

Note the time taken

### Async
Now to Benchmark the async version

1. Run the server
```gunicorn workindiaconsumer.wsgi -b 0.0.0.0:9008 --workers 10 --limit-request-line 4094 --limit-request-fields 100 --timeout 300 --log-level debug```

2. Start the consumer
```sh
python manage.py async_task

```

3. Now run the script to produce 1000 messages
```sh
python manage.py shell
```

Once inside the shell, run the following commands

- `from workindiaconsumer.utils import *`
- `main()`

Note the time taken


