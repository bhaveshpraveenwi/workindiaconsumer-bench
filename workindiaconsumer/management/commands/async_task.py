import time

from django.core.management import BaseCommand

from workindiaconsumer.kafka import PConsumer, async_consumer


class Command(BaseCommand):
    help = 'python3 manage.py emailbatchconsumer'

    def handle(self, *args, **options):
        async_consumer()
