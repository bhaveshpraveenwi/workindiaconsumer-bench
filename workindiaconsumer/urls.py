import time

from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(["GET"])
def test(request):
    time.sleep(0.1)
    return Response(status=status.HTTP_200_OK)


urlpatterns = [
    # Examples:
    # url(r'^$', 'workindiaconsumer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^test/', test)
]
