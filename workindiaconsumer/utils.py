import random
import threading

from workindiaconsumer.kafka import PPayload, PProducer


no_of_threads = 10
no_of_message_to_send = 1000


def produce(thread_no):
    no_of_msgs_to_send_per_thread = no_of_message_to_send/no_of_threads
    for i in range(int(no_of_msgs_to_send_per_thread)):
        # number = random.randint(1, 10000)
        message = PPayload.get_message(i)

        print(f"Producing message no {i} from {thread_no}")
        PProducer().produce(topic="bench.task", message=message)

def main():
    threads = []

    for index in range(no_of_threads):
        x = threading.Thread(target=produce, args=(index,))
        threads.append(x)

    for index in range(no_of_threads):
        threads[index].start()

    for index in range(no_of_threads):
        threads[index].join()

if __name__ == "__main__":
    main()