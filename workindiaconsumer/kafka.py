import json
import time
from pprint import pprint
from typing import Type

import requests
from django.conf import settings
from wiadapters.kafka.baseconsumer import BaseConsumer
from wiadapters.kafka.baseproducer import BaseProducer
from wiadapters.kafka.types import Payload, TPayload, Message
from wiadapters.utils.singleton import Singleton


class PPayload(Payload):
    def __init__(
            self,
            number: str,
    ):
        self.number = number

    @classmethod
    def to_instance(cls: Type[TPayload], *args, **kwargs) -> TPayload:
        return cls(*args, **kwargs)

    def to_dict(self):
        return {
            "number": self.number,
        }

    @staticmethod
    def get_message(number):
        payload = PPayload(number=number)
        return Message(payload=payload, message_type="random_msg")

@Singleton
class PProducer(BaseProducer):
    producer_conf = {
        "bootstrap.servers": settings.KAFKA_CLUSTER["brokers"],
        "queue.buffering.max.ms": 1500,
        "queue.buffering.max.messages": 500_000,
        "message.send.max.retries": 5,
        "retry.backoff.ms": 60_000
    }


class PConsumer(BaseConsumer):
    config = {
        "bootstrap.servers": settings.KAFKA_CLUSTER["brokers"],
        "group.id": "bench_sync_group",
        "enable.auto.commit": True,
        'auto.offset.reset': 'latest',
        "auto.commit.interval.ms": 10000,
    }
    topics: list = ["bench.task"]
    payload_class = PPayload
    message_class = Message
    message_count = 0
    start_time = None
    end_time = None

    def handle_message(self, message):
        print(f"Received message with number: {message.payload.number}")
        if PConsumer.message_count == 0 and PConsumer.start_time is None:
            PConsumer.start_time = time.perf_counter()

        PConsumer.message_count += 1
        number = message.payload.number
        response = requests.get("http://127.0.0.1:9008/test/")

        if PConsumer.message_count == 1000:
            print(f"--------------------------------\n"
                  f"Took {time.perf_counter() - PConsumer.start_time} secs to run.\n"
                  f"----------------------------------")
            raise KeyboardInterrupt



from aiokafka import AIOKafkaProducer, AIOKafkaConsumer
import asyncio
import aiohttp


class Temp:
    message_num = 0
    completed_msg_num = 0
    start_time = None


async def post_data(session):
    async with session.get("http://127.0.0.1:9008/test/") as resp:
        Temp.completed_msg_num += 1

        if Temp.completed_msg_num == 1000:
            print(f"Time taken {time.perf_counter() - Temp.start_time} secs with completed {Temp.completed_msg_num}")



session = aiohttp.ClientSession()

def async_consumer():
    loop = asyncio.get_event_loop()

    async def consume():
        consumer = AIOKafkaConsumer(
            bootstrap_servers='localhost:9092',
            group_id="bench_async_group",
            enable_auto_commit=True,
            auto_commit_interval_ms=10000,
            auto_offset_reset="latest",
            loop=loop)

        # Get cluster layout and join group `my-group`
        print(f"About to start consumer.")
        await consumer.start()

        consumer.subscribe(topics=["bench.task"])

        try:
            # Consume messages
            async for msg in consumer:
                value = msg.value
                message = json.loads(value)
                if not message:
                    continue

                payload = message.get("payload")
                if not payload:
                    continue

                print(f"Received message with number: {payload['number']}")
                if Temp.message_num == 0:
                    Temp.start_time = time.perf_counter()

                # make the request
                loop.create_task(post_data(session))

                Temp.message_num += 1

                if Temp.message_num == 1000:
                    await consumer.stop()
                    break

        finally:
            # Will leave consumer group; perform autocommit if enabled.
            await consumer.stop()

    print("Run until complete")
    loop.run_until_complete(consume())

    pending = asyncio.Task.all_tasks(loop=loop)

    all_groups = asyncio.gather(*pending, return_exceptions=True)
    results = loop.run_until_complete(all_groups)
    loop.close()
    pprint(results)


